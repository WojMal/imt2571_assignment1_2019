<?php
//Gruppe 88: Wojciech Malecki, Sander Høgli, Sondre Elstad//
//////////////////Sammarbeid med gruppe 100////////////////////////

/** The Model implementation of the IMT2571 Assignment #1 MVC-example, storing
  * data in a MySQL database using PDO.
  * @author Rune Hjelsvold
  * @see http://php-html.net/tutorials/model-view-controller-in-php/
  *      The tutorial code used as basis.
  */

require_once("AbstractEventModel.php");
require_once("Event.php");
require_once("dbParam.php");

/** The Model is the class holding data about a archive of events.
  * @todo implement class functionality.
  */
class DBEventModel extends AbstractEventModel
{
    protected $db = null;

    /**
      * @param PDO $db PDO object for the database; a new one will be created if
      *                no PDO object is passed
      * @todo Complete the implementation using PDO and a real database.
      * @throws PDOException
      */
    public function __construct($db = null)
    {
        if ($db) {
            $this->db = $db;
        } else
        {
           try
            {
				
            //connects to the database
			
             $this->db = new PDO( 'mysql:host=' . DB_HOST . ';dbname=' .
							DB_NAME . ';charset=utf8', DB_USER, DB_PWD,
                        array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
             
            }
            catch(PDOException $e)//if something goes wrong, displays error msg 
            {
             echo "Error: " . $e->getMessage();
            }
        }
    }

    /** Function returning the complete list of events in the archive. Events
      * are returned in order of id.
      * @return Event[] An array of event objects indexed and ordered by id.
      * @todo Complete the implementation using PDO and a real database.
      * @throws PDOException
      */
    public function getEventArchive()
    {
        $eventList = array();
    
        $dbh = $this->db->prepare('SELECT * FROM event');
        $dbh ->execute();
        
        $eventList = $dbh->fetchAll(PDO::FETCH_OBJ);
     
                /*Retrieves an array of objects from the database. 
				  We did not know how to do it one-by-one, 
				  so we fetched all at the same time.*/
        
        return $eventList;
    }

    /** Function retrieving information about a given event in the archive.
      * @param integer $id the id of the event to be retrieved
      * @return Event|null The event matching the $id exists in the archive;
      *         null otherwise.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      */
    public function getEventById($id)
    {
        
        Event::verifyId($id);
        
        
        $stmt = $this->db->prepare("SELECT * FROM `event` WHERE id=?");
        $stmt->execute([$id]);
        $event = $stmt->fetch(PDO::FETCH_OBJ);  //Fetches the choosen record from the database
        
       return $event;
    }

    /** Adds a new event to the archive.
      * @param Event $event The event to be added - the id of the event will be set after successful insertion.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      * @throws InvalidArgumentException If event data is invalid
      */
    public function addEvent($event)
    {
       
          
          $event->verify();  //verifies the data inserted by the user
        
            $stmt = $this->db->prepare("INSERT INTO `event` 
									  (`title`, `date`, `description`) 
									    VALUES(:title, :date, :desc)");
            $stmt->execute([':title' => $event->title,
                            ':date' => $event->date,
                            ':desc' => $event->description]);
            
            $event->id = $this->db->lastInsertId();//sets id to the number of last inserted row
    }

    /** Modifies data related to a event in the archive.
      * @param Event $event The event data to be kept.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      * @throws InvalidArgumentException If event data is invalid
     */
    public function modifyEvent($event)
    {
         
          $event->verify();  //verifies the data inserted by the user

        $stmt = $this->db->prepare('UPDATE `event` SET `title`=:title,
					`date`=:date, `description`=:desc WHERE `id`=:id');
					
            $stmt->execute([
            ':title' => $event->title,
            ':date' => $event->date,
            ':desc' => $event->description,
            ':id' => $event->id]);
       
    }

    /** Deletes data related to a event from the archive.
      * @param $id integer The id of the event that should be removed from the archive.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
     */
    public function deleteEvent($id)
    {
       
			Event::verifyId($id);//verifies the id
			
            $stmt = $this->db->prepare('DELETE FROM event WHERE id=?');
            $stmt->execute([$id]);  
       
    }
}
